# -*- coding: utf-8 -*-
"""
Created on Thu Sep 27 18:46:41 2018

@author: Konstantin Petrov

генерация csv для визуализации статистики по RTB
"""

import requests
from urllib.parse import urlencode
import pandas as pd
import datetime
import adsense_rtb
import set_types
import re
import os.path
import sys
import logging


if 'win' in sys.platform:
    pth=os.getcwd()+'\\'
else:
    pth=os.getcwd()+"/flask/public_html/ads/"
sites_pm_file=pth+"sites_pm.csv"
client_secrets_file=pth+"client_secrets.json"
MyCreds_file=pth+"MyCreds.dat"
log_txt=pth+"log.txt"
logger = logging.getLogger()
logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.INFO, filename = log_txt)


period='35days'
fromDate=str(datetime.date.today()-datetime.timedelta(days=35))
toDate=str(datetime.date.today())

token = ''
 
params = (
    ('oauth_token', token),
    ('period', period),
    ('lang', 'ru'), 
    ('level', 'advnet_context_on_site_rtb'),
    ('field', 'domain'),
    ('field', 'data'),
    ('field', 'rtb_block_shows'),
    ('field', 'rtb_block_direct_shows'),
    ('field', 'rtb_block_direct_clicks'),
    ('field', 'rtb_block_hits_own_adv'),
    ('field', 'rtb_block_shows_own_adv'),
    ('field', 'rtb_block_all_hits'),
    ('field', 'rtb_block_cover_ratio'),
    ('field', 'rtb_block_cpm_partner_wo_nds'),
    ('field', 'rtb_block_cpmh_partner_wo_nds'),
    ('field', 'rtb_block_hits_unsold'),
    ('field', 'rtb_block_rpm_partner_wo_nds'),
    ('field', 'rtb_block_visibility'),
    ('field', 'rtb_block_winrate'),
    ('field', 'rtb_partner_wo_nds'),
    ('filter_values', '2'),
    ('dimension_field', 'date|day'),
    ('entity_field', 'domain'),
    ('entity_field', 'data'),
    ('entity_field', 'caption'),
    #('entity_field', 'caption'),
)
 
url = 'https://partner2.yandex.ru/api/statistics/get.json?' + urlencode(params)
 
#print (url)
 
response = requests.get(url)
 

if 'message' in response.json():
    print("\n\n"+response.json()['message'])
    logging.info("\n\n"+response.json()['message'])

print("Получаю данные с Яндекс")
logging.info("Получаю данные с Яндекс")
yandex_rtb=pd.DataFrame(response.json()['data']['data'])
yandex_rtb=set_types.set_types(yandex_rtb)    

print("Получаю данные с Adsense RTB")
logging.info("Получаю данные с Adsense RTB")
adsens_rtb=adsense_rtb.get_adsens_rtb(fromDate,toDate)
adsens_rtb=set_types.set_types(adsens_rtb)
adsens_rtb.to_csv(pth+'adsens_rtb_raw.csv', encoding='utf8',decimal=",", header=True)

print("Получаю данные с Adsense по сайтам")
logging.info("Получаю данные с Adsense по сайтам")
adsens_sites=adsense_rtb.get_adsens_sites_earnings(fromDate,toDate)
adsens_sites=set_types.set_types(adsens_sites)


domains=pd.DataFrame(pd.unique(pd.concat([adsens_rtb['DOMAIN_NAME'],adsens_sites['DOMAIN_NAME'],yandex_rtb['domain']])),columns=['DOMAIN'])


sites_pm=pd.DataFrame({'pm':['Рук1',
                             'Рук2',
                             'Рук3',                           
                             ],
                           'DOMAIN':['site1.ru',
                                     'site12.ru',
                                     ]
                           })
                           
           
print("Обрабатываю Турбо")
logging.info("Обрабатываю Турбо")

domains_all=list(pd.Series(list(set(adsens_rtb['DOMAIN_NAME']))))
good_domains=list(set(sites_pm['DOMAIN']))

bad_domains=pd.DataFrame(columns=domains_all,index=good_domains)
for rtb_domain in domains_all:
    for site in good_domains:
        if site in rtb_domain:
            bad_domains.loc[site][rtb_domain]=None
        else:
            bad_domains.loc[site][rtb_domain]=True
bad_domains=bad_domains.dropna(how='any',axis=1)
maybe_turbo=list(bad_domains.columns)
del(bad_domains)


turbo_ads=adsens_rtb[adsens_rtb['DOMAIN_NAME'].isin(maybe_turbo)]

turbo_i=[]
for i in range(len(adsens_rtb)):
    if adsens_rtb['DOMAIN_NAME'].iloc[i] in maybe_turbo:
        turbo_i.append(i)
del(maybe_turbo)
      
turbo_ads_df=adsens_rtb.iloc[turbo_i]
turbo_ads_df=turbo_ads_df[turbo_ads_df['AD_UNIT_NAME'].str.contains(":")==True]




new_domain_name=pd.DataFrame(columns=['DOMAIN_NAME'])
for i in pd.unique(turbo_ads_df.index):
    ban_name=turbo_ads_df.loc[i]['AD_UNIT_NAME']
    try:
        name=re.split(r":",ban_name)[0].strip(' \t\n\r')
        if(name in list(sites_pm['DOMAIN'])):
            new_domain_name.loc[i]=name
    except Exception:
        pass
del(turbo_ads_df)
        
        
 
dfc=adsens_rtb.loc[new_domain_name.index]
adsens_rtb=adsens_rtb.drop(new_domain_name.index)
dfc['DOMAIN_NAME']=new_domain_name['DOMAIN_NAME']
adsens_rtb=pd.concat([adsens_rtb,dfc])
del(dfc)



print("Сохраняю")
logging.info("Сохраняю")
adsens_rtb.to_csv(pth+'adsens_rtb.csv', encoding='utf8',decimal=",", header=True)
adsens_sites.to_csv(pth+'adsens_sites.csv', encoding='utf8',decimal=",", header=True)
yandex_rtb.to_csv(pth+'yandex_rtb.csv', encoding='utf8',decimal=",", header=True)
domains.to_csv(pth+'domains.csv', encoding='utf8',decimal=",", header=True)
sites_pm.to_csv(pth+'sites_pm.csv', encoding='utf8',decimal=",", header=True)
print("Готово")
logging.info("Готово\n-------------------\n")
